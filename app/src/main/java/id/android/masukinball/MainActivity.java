package id.android.masukinball;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.games.Games;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, Runnable {

    //frame
    private FrameLayout gameFrame;
    private int frameHeight, frameWidth, initialFrameWidth;
    private LinearLayout startLayout;

    //image
    private ImageView box, black, orange, pink;
    private Drawable imageBoxRight, imageBoxLeft;

    //Size
    private int boxSize;

    //Button
    private Button mHighScore;
    private Button credit;
    private Button exit;

    //position
    private float boxX, boxY;
    private float blackX, blackY;
    private float orangeX, orangeY;
    private float pinkX, pinkY;

    //score
    private TextView scoreLabel, highScoreLabel;
    private int score, highScore, timeCount;
    private SharedPreferences settings;

    //Class
    private Timer timer;
    private Handler handler = new Handler();
    private SoundPlayer soundPlayer;

    //status
    private boolean start_flg = false;
    private boolean action_flg =false;
    private boolean pink_flg =false;

    //play service declaration
    private static final String TAG = "MainActivity";
    private static final int RC_SIGN_IN = 9001;
    private static final int RC_LEADERBOARD_UI = 9004;
    private static final int RC_ACHIEVEMENT_UI = 9003;
    final public int BUTTON_SIGN_IN = 2;
//    final public int BUTTON_SIGN_OUT = 1;

    SignInButton signInButton;
    private GoogleSignInAccount googleSignInAccount = null;
    private GoogleSignInClient googleSignInClient;
    private GoogleApiClient apiClient;
    private AdView adView;
    private Object GoogleSignInClient;
    private Object Leaderboard;

    InterstitialAd interstitialAd;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        soundPlayer = new SoundPlayer(this);

        gameFrame = findViewById(R.id.gameFrame);
        startLayout = findViewById(R.id.startLayout);
        signInButton = findViewById(R.id.sign_in_button);
        mHighScore = findViewById(R.id.high_score);
//        credit = findViewById(R.id.Credits);
        box = findViewById(R.id.box);
        black = findViewById(R.id.black);
        orange = findViewById(R.id.orange);
        pink = findViewById(R.id.pink);
        scoreLabel = findViewById(R.id.scoreLabel);
        exit = findViewById(R.id.quit);

        //highScoreLabel = findViewById(R.id.highScoreLabel);

        Glide.with(this)
                .load(R.drawable.kanann)
                .into(box);

        Glide.with(this)
                .load(R.drawable.hitam)
                .into(black);

        Glide.with(this)
                .load(R.drawable.silver)
                .into(orange);

        Glide.with(this)
                .load(R.drawable.emas)
                .into(pink);


        // Sample AdMob app ID: ca-app-pub-3940256099942544~3347511713
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");

        mHighScore = findViewById(R.id.high_score);

        imageBoxLeft = getResources().getDrawable(R.drawable.kirii);
        imageBoxRight = getResources().getDrawable(R.drawable.kanann);

        //High Score
        //settings = getSharedPreferences("GAME_DATA", Context.MODE_PRIVATE);
        //highScore = settings.getInt("HIGH_SCORE", 0);
        //highScoreLabel.setText("High Score : " + highScore);

        signInButton.setOnClickListener(this);
        mHighScore.setOnClickListener(this);
//        credit.setOnClickListener(this);
        exit.setOnClickListener(this);

        //InterstitialAd
        //Interstitial
        interstitialAd = new InterstitialAd(this);
        interstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        interstitialAd.loadAd(new AdRequest.Builder().build());
        interstitialAd.setAdListener(new AdListener(){
            private void onAdClose(){
                super.onAdClosed();

                interstitialAd.loadAd(new AdRequest.Builder().build());
            }
        });


        //implement API
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN)
                .requestEmail()
                .build();

        apiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        if (GoogleSignIn.getLastSignedInAccount(this) != null) {
            Games.getLeaderboardsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .submitScore(getString(R.string.leaderboard), 1337);

            Games.getAchievementsClient(this, GoogleSignIn.getLastSignedInAccount(this))
                    .unlock(getString(R.string.achivement_welcome));
        }

    }

    public void showAds(){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(interstitialAd != null && interstitialAd.isLoaded())
                    interstitialAd.show();
            }
        });
    }

    private void SignIn(){
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(apiClient);
        startActivityForResult(intent, RC_SIGN_IN);
    }

    private void showLead(){
        Games.getLeaderboardsClient(this, (GoogleSignIn.getLastSignedInAccount(this)))
                .getLeaderboardIntent(getString(R.string.leaderboard))
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_LEADERBOARD_UI);
                    }
                });
    }

    private void showAchievements() {
        Games.getAchievementsClient(this, (GoogleSignIn.getLastSignedInAccount(this)))
                .getAchievementsIntent()
                .addOnSuccessListener(new OnSuccessListener<Intent>() {
                    @Override
                    public void onSuccess(Intent intent) {
                        startActivityForResult(intent, RC_ACHIEVEMENT_UI);
                    }
                });
    }

    public void changePos(){

        //Add timeCount
        timeCount += 20;

        //Orange
        orangeY += 12;

        float orangeCenterX = orangeX + orange.getWidth() / 2;
        float orangeCenterY = orangeY + orange.getHeight() / 2;

        if (hitCheck(orangeCenterX, orangeCenterY)) {
            orangeY = frameHeight + 100;
            score += 10;
            soundPlayer.playHitOrangeSound();
        }

        if (orangeY > frameHeight) {
            orangeY = -100;
            orangeX = (float) Math.floor(Math.random() * (frameWidth - orange.getWidth()));
        }
        orange.setX(orangeX);
        orange.setY(orangeY);

        //pink
        if (!pink_flg && timeCount % 10000 == 0) {
            pink_flg = true;
            pinkY = -20;
            pinkX = (float) Math.floor(Math.random() * (frameWidth - pink.getWidth()));
        }

        if (pink_flg) {
            pinkY += 20;

            float pinkCenterX = pinkX + pink.getWidth() / 2;
            float pinkCenterY = pinkY + pink.getWidth() / 2;

            if (hitCheck(pinkCenterX, pinkCenterY)) {
                pinkY = frameHeight + 30;
                score += 30;
                //change frameWidth
                if (initialFrameWidth > frameWidth * 110 /100) {
                    frameWidth = frameWidth * 110 / 100;
                    changeFrameWidth(frameWidth);
                }
                soundPlayer.playHitPinkSound();

            }

            if (pinkY > frameHeight) pink_flg = false;
            pink.setX(pinkX);
            pink.setY(pinkY);
        }

        //Black
        blackY += 18;

        float blackCenterX = blackX + black.getWidth() / 2;
        float blackCenterY = blackY + black.getHeight() /2;

        if (hitCheck(blackCenterX, blackCenterY)) {
            blackY = frameHeight + 100;

            //change FrameWidth
            frameWidth = frameWidth * 80/100;
            changeFrameWidth(frameWidth);
            soundPlayer.playHitBlackSound();
            if (frameWidth <= boxSize) {
                showAds();
                gameOver();

            }
        }

        if (blackY > frameHeight) {
            blackY = -100;
            blackX = (float) Math.floor(Math.random() * (frameWidth - black.getWidth()));

        }

        black.setX(blackX);
        black.setY(blackY);

        //Move Box
        if (action_flg) {
            // Touching
            boxX += 14;
            box.setImageDrawable(imageBoxRight);
        } else {
            //releasing
            boxX -= 14;
            box.setImageDrawable(imageBoxLeft);
        }

        //Check box position
        if (boxX < 0) {
            boxX = 0;
            box.setImageDrawable(imageBoxRight);
        }
        if (frameWidth - boxSize < boxX) {
            boxX = frameWidth - boxSize;
            box.setImageDrawable(imageBoxLeft);
        }

        box.setX(boxX);

         scoreLabel.setText("Score : " + score);
    }

    public boolean hitCheck(float x, float y) {
        if (boxX <= x && x <= boxX + boxSize && boxY <= y && y <= frameHeight) {
            return true;
        }
        return  false;
    }

    public void changeFrameWidth(int frameWidth) {
        ViewGroup.LayoutParams params = gameFrame.getLayoutParams();
        params.width = frameWidth;
        gameFrame.setLayoutParams(params);
    }

    public void gameOver() {
        //Stop timer
        timer.cancel();
        timer = null;
        start_flg = false;

        //Before Showing startLayout, sleep 1 second.
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        changeFrameWidth(initialFrameWidth);

        startLayout.setVisibility(View.VISIBLE);
        box.setVisibility(View.INVISIBLE);
        black.setVisibility(View.INVISIBLE);
        orange.setVisibility(View.INVISIBLE);
        pink.setVisibility(View.INVISIBLE);

        // Update High Score
        //if (score > highScore) {
         //   highScore = score;
         //   highScoreLabel.setText("High Score : " + highScore);

          //  SharedPreferences.Editor editor = settings.edit();
          //  editor.putInt("HIGH_SCORE", highScore);
         //   editor.commit();
        //}
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (start_flg){
            if (event.getAction() == MotionEvent.ACTION_DOWN){
                action_flg = true;
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                action_flg = false;
            }
        }
        return true;
    }

    public void startGame(View view){
        start_flg = true;
        startLayout.setVisibility(View.INVISIBLE);

        if (frameHeight == 0) {
            frameHeight = gameFrame.getHeight();
            frameWidth = gameFrame.getWidth();
            initialFrameWidth = frameWidth;

            boxSize = box.getHeight();
            boxX = box.getX();
            boxY = box.getY();
        }

        frameWidth = initialFrameWidth;

        box.setX(0.0f);
        black.setY(3000.0f);
        orange.setY(3000.0f);
        pink.setY(3000.0f);

        blackY =black.getY();
        orangeY = orange.getY();
        pinkY = pink.getY();

        box.setVisibility(View.VISIBLE);
        black.setVisibility(View.VISIBLE);
        orange.setVisibility(View.VISIBLE);
        pink.setVisibility(View.VISIBLE);

        timeCount = 0;
        score = 0;
        scoreLabel.setText("Score : 0");

        timer = new Timer();
        timer.schedule(new TimerTask(){
            @Override
            public void run() {
                if (start_flg) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            changePos();
                        }
                    });
                }
            }
        }, 0, 20);

    }

   // public void creditGame(View view){

    //}

//    public void quitGame(View view){
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP){
//            finishAndRemoveTask();
//        }else {
//            finish();
//        }
//    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.sign_in_button:
                Log.d(TAG, "BUTTON_SIGN_IN");
                Toast.makeText(this, "SIGN IN", Toast.LENGTH_SHORT).show();

                SignIn();
                showAchievements();
                break;
            case R.id.high_score:
                Log.d(TAG,"LEADERBOARD");
                Toast.makeText(this, "LEADERBOARD", Toast.LENGTH_SHORT).show();

//                startActivity(new Intent(this, HighScoreActivity.class));
                showLead();
                break;

//            case R.id.Credits:
//                Log.d(TAG,"CREDIT");
//                Toast.makeText(this, "CREDIT", Toast.LENGTH_SHORT).show();
//
//                startActivity(new Intent(this, Kredit.class));
//                break;
            case R.id.quit:
                Log.d(TAG, "QUIT");
                Toast.makeText(this, "QUIT", Toast.LENGTH_SHORT).show();

                finish();
                break;
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==RC_SIGN_IN){
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(task);

            Toast.makeText(this, "wew", Toast.LENGTH_SHORT).show();
        }
    }

//    private void handleSignInResult(GoogleSignInResult result){
//        if(result.isSuccess()){
//            gotoGame();
//        }else{
//            Toast.makeText(getApplicationContext(),"Sign in cancel",Toast.LENGTH_LONG).show();
//        }
//    }
//    private void gotoGame(){
//        Intent intent = new Intent(MainMenuActivity.this, GameView.class);
//        startActivity(intent);
//    }

    private void handleSignInResult(Task<GoogleSignInAccount> completedTask) {
        try {
            GoogleSignInAccount account = completedTask.getResult(ApiException.class);

            // Signed in successfully, show authenticated UI.
            updateUI(account);
        } catch (ApiException e) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w(TAG, "signInResult:failed code=" + e.getStatusCode());
            updateUI(null);
        }
    }

    private void updateUI(@Nullable GoogleSignInAccount account) {
        if (account != null) {


            findViewById(R.id.sign_in_button).setVisibility(View.GONE);
//            findViewById(R.id.sign_out_button).setVisibility(View.VISIBLE);
        } else {
//            mStatusText.setText(R.string.signed_out);

            findViewById(R.id.sign_in_button).setVisibility(View.VISIBLE);
//            findViewById(R.id.sign_out_button).setVisibility(View.GONE);
        }
    }

    @Override
    public void run() {

    }
}













